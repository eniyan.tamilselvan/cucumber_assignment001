package com.testng;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/features",glue="com.testng")

public class TestNGrunner extends AbstractTestNGCucumberTests {

}
