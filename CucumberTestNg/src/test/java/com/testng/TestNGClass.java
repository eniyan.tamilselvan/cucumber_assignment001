package com.testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestNGClass {
	static WebDriver driver = null;
	
	@Given("Open the browser")
	public void open_the_browser() {
		driver = new ChromeDriver();
		driver.get(" https://demowebshop.tricentis.com");
		driver.manage().window().maximize();

	}

	@Then("The homepage of website is get displayed")
	public void the_homepage_of_website_is_get_displayed() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter the valid Email {string}")
	public void enter_the_valid_email(String Email) {
		System.out.println("Enter the email");
		driver.findElement(By.id("Email")).sendKeys(Email);

	}

	@When("Enter the valid Password {string}")
	public void enter_the_valid_password(String Pass) {
		System.out.println("Enter the password");
		driver.findElement(By.id("Password")).sendKeys(Pass);

	}

	@When("Click Login option")
	public void click_login_option() {
		System.out.println("Click the login button");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

	}

	@Then("Home page should be display successfully")
	public void home_page_should_be_display_successfully() {
		System.out.println("Home Page displayed");

	}

	@Then("close the browser")
	public void close_the_browser() {
		System.out.println("close the browser");
		driver.close();
	}

}
